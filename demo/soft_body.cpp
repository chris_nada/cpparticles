// Demonstrates the use of springs to create a soft body.
#include <SFML/Graphics.hpp>
#include <cpparticles.hpp>

using namespace cpparticles; // just for this demo, don't do this in your code

int main() {
    
    // Set up the environment.
    Environment* env = new Environment(800, 600);
    Joint* selectedJoint = nullptr;
    
    // Create the main window.
    sf::RenderWindow window(sf::VideoMode(env->getWidth(), env->getHeight()), "Soft Body Simulation");
    window.setFramerateLimit(60);
    
    // Add Joint for the soft body to the environment.
    int size = 10;
    int mass = 600;
    int speed = 0;
    int angle = 0;
    float elasticity = 0.1;
    
    Joint *p1 = env->addJoint(300, 300, size, mass, speed, angle, elasticity);
    Joint *p2 = env->addJoint(500, 300, size, mass, speed, angle, elasticity);
    Joint *p3 = env->addJoint(500, 500, size, mass, speed, angle, elasticity);
    Joint *p4 = env->addJoint(300, 500, size, mass, speed, angle, elasticity);
    
    // Connect Joint using springs to create the soft body.
    int length = 200;
    int strength = 50;
    
    env->addSpring(p1, p2, length, strength);
    env->addSpring(p2, p3, length, strength);
    env->addSpring(p3, p4, length, strength);
    env->addSpring(p4, p1, length, strength);
    env->addSpring(p1, p3, length, strength);
    env->addSpring(p2, p4, length, strength);
    
    while (window.isOpen()) {
        
        // Process events.
        sf::Event event;
        while (window.pollEvent(event)) {
            
            // Close window: exit.
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            
            // Escape: exit.
            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Escape) {
                    window.close();
                }
            }
            
            // Left mouse button: select Joint.
            if (event.type == sf::Event::MouseButtonPressed) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    float mouseX = event.mouseButton.x;
                    float mouseY = event.mouseButton.y;
                    selectedJoint = env->getJoint(mouseX, mouseY);
                }
            }
            
            if (event.type == sf::Event::MouseButtonReleased) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    selectedJoint = nullptr;
                }
            }
        }
        
        // Clear the window.
        window.clear();
        
        // Update the environment.
        env->update();
        
        // Move the selected Joint to the cursor's position.
        if (selectedJoint) {
            float mouseX = sf::Mouse::getPosition(window).x;
            float mouseY = sf::Mouse::getPosition(window).y;
            selectedJoint->moveTo(mouseX, mouseY);
        }
        
        // Draw springs.
        for (int i=0; i<env->getSprings().size(); i++) {
            auto& spring = env->getSprings()[i];
            sf::Vertex line[] =
            {
                sf::Vertex(sf::Vector2f(spring->getP1()->getX(), spring->getP1()->getY())),
                sf::Vertex(sf::Vector2f(spring->getP2()->getX(), spring->getP2()->getY()))
            };
            window.draw(line, 2, sf::Lines);
        }
        
        // Update the window.
        window.display();
    }
    
    return EXIT_SUCCESS;
}
