This is a fork of https://github.com/faishasj/cpparticles incorporating changes from Chicorak's fork (most prominently the QuadTree and renaming Particle to Joint) plus bugfixes and refactoring - putting everything in `namespace cpparticles`, making it more safe to use as a library and removing all usages of owning raw pointers, which btw had several bugs in them - don't use those in C++, if you're able to `#include <memory>`.

# CPParticles
A small 2D physics library written in C++ with included SFML demo programs. Made for practice and funsies with some guidance from 
[this Python tutorial](http://www.petercollingridge.co.uk/tutorials/pygame-physics-simulation/).

Also the Line and QuadTree guidance and learning [from this](https://github.com/OneLoneCoder/olcPixelGameEngine/blob/master/Videos/OneLoneCoder_PGE_Balls2.cpp).

## Installation

### CMake

1. Put cpparticles in a subdirectory of your project in which you wish it to use. 

2. Add these lines to your `CMakeLists.txt`
```cmake
    ...
    add_subdirectory(cpparticles)
    ...
    include_directories(cpparticles/include)
    ...
    target_link_libraries(PROJECT_NAME PRIVATE cpparticles)
    ...
```
+ Change `PROJECT_NAME` to the name of your cmake project.
+ If you put cpparticles in a different folder than `/cpparticles`, then change the first and second cmake command accordingly.


> ### Old (deprecated) installation instructions
> To use CPParticles in your project, include the `include` and `src` > folders in your project directory. The `cpparticles.hpp` header file includes all the header files in the library:
> ```cpp
> #include "include/cpparticles.hpp"
> ```
> Alternatively, you may also choose to include the individual header files. 

## Demos
This repository includes three demo files for your viewing pleasure (and also, in the meantime to serve as examples on how to use this library and 
demonstrate its capabilities because this readme is yet to be made fully extensive).

> Compiling the demo programs requires [SFML](https://www.sfml-dev.org/) to be installed.

### collisions.cpp
This program demonstrates particle physics in the library within the standard environment.

![Collisions demo](demo/gif/collisions.gif)

### gas_cloud.cpp
This program demonstrates how changing the environment attributes in the library can be used to simulate a gas cloud.

![Gas cloud demo](demo/gif/gas_cloud.gif)

### soft_body.cpp
This program demonstrates the use of springs to create a soft body.

![Soft body demo](demo/gif/soft_body.gif)

## License

This project is licensed under the MIT license. See [LICENSE.md](LICENSE.md) for details.
