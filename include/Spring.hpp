#pragma once

#include <math.h>
#include "Joint.hpp"

namespace cpparticles {

// Handles the movement and forces acting upon the spring.
class Spring {

public:
    Spring(Joint* p1, Joint* p2, float restlength=50, float strength=0.5);
    Joint* getP1() { return p1; }
    Joint* getP2() { return p2; }
    void update();
    
protected:
    float length;
    float strength;
    Joint* p1;
    Joint* p2;
    
};

}
