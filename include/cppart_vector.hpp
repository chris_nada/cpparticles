#pragma once

#include <math.h>

namespace cpparticles {

    // Contains direction (angle) and magnitude (speed).
    struct Vector {
        float angle;
        float speed;
    };

}
