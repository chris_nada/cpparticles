// Header for the Environment class.
#ifndef environment_hpp
#define environment_hpp

#ifndef M_PI
#define M_PI 3.14159265359 
#endif

#include <math.h>
#include <random>
#include <algorithm>
#include <memory>
#include "Joint.hpp"
#include "Line.hpp"
#include "Spring.hpp"
#include "QuadTree.hpp"

namespace cpparticles {

// Handles all interaction between Joints, springs and attributes within the environment.
class Environment {

public:

    Environment(int width, int height, Vector GravVector = {M_PI, 0.2});

    int getHeight() { return height; }
    int getWidth() { return width; }

    Joint* addJoint(); // adds a Random Joint aka Particle
    Joint* addJoint(float x, float y, float size=10, float mass=100, float speed=0, float angle=0, float elasticity=0.9);
    Joint* getJoint(float x, float y);

    cpparticles::Line* addLine(float StartX, float StartY, float EndX, float EndY, float LineWidth);
    cpparticles::Line* getLine(float x, float y);

    Spring* addSpring(Joint* p1, Joint* p2, float length=50, float strength=0.5);

    const std::vector<std::unique_ptr<Joint>>& getJoints() { return joints; }
    const std::vector<std::unique_ptr<Line>>& getLines() { return lines; }
    const std::vector<std::unique_ptr<Spring>>& getSprings(){ return springs;}
    
    void update();

    void setAirMass(float a) { airMass = a; }
    void setAllowAccelerate(bool setting) { allowAccelerate = setting; }
    void setAllowAttract(bool setting) { allowAttract = setting; }
    void setAllowBounce(bool setting) { allowBounce = setting; }
    void setAllowCollide(bool setting) { allowCollide = setting; }
    void setAllowCombine(bool setting) { allowCombine = setting; }
    void setAllowDrag(bool setting) { allowDrag = setting; }
    void setAllowMove(bool setting) { allowMove = setting; }
    void setElasticity(float e) { elasticity = e; }

    void bounce(Joint* joint);
    void removeJoint(Joint* joint);
    void removeSpring(Spring* spring);

protected:

    const int height;
    const int width;
    const float Stable = 0.15f;
    bool allowAccelerate = true;
    bool allowAttract = false;
    bool allowBounce = true;
    bool allowCollide = true;
    bool allowCombine = false;
    bool allowDrag = true;
    bool allowMove = true;
    float airMass = 0.2;
    float elasticity = 0.75;
    std::vector<std::unique_ptr<Joint>> joints;
    std::vector<std::unique_ptr<Spring>> springs;
    std::vector<std::unique_ptr<Line>> lines;
    std::vector<std::unique_ptr<Collidable>> collidables;
    std::unique_ptr<QuadTree> quadTree;
    Vector gravity;

};

#endif // environment_hpp

}
